package com.ena.muratspahic.materijali_java.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import com.ena.muratspahic.materijali_java.R;
import com.ena.muratspahic.materijali_java.adapters.SongAdapter;
import com.ena.muratspahic.materijali_java.database.MusicianDBHelper;
import com.ena.muratspahic.materijali_java.model.Song;
import com.ena.muratspahic.materijali_java.utils.Constants;

import java.util.ArrayList;
import java.util.List;

public class ChildFragmentTopSongs extends Fragment {

    private List<Song> mSongs = new ArrayList<>();

    private ListView topSongsListView;

    private String musicianName;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.child_fragment_five, container, false);

        TextView title = view.findViewById(R.id.textView);
        title.setText(getString(R.string.top_songs));

        topSongsListView = view.findViewById(R.id.listView);

        if (getArguments() != null && getArguments().containsKey(Constants.MUSICIAN_SPOTIFY_ID)) {
            String musicianId = getArguments().getString(Constants.MUSICIAN_SPOTIFY_ID);
            if (musicianId != null) {
                // Get from database and set album adapter
                try (MusicianDBHelper db = new MusicianDBHelper(getActivity())) {
                    mSongs = db.getAllSongsFromMusician(musicianId);
                    musicianName = db.getMusicianName(musicianId);
                    setSongsAdapter();

                } finally {
                    mSongs = new ArrayList<>();
                    musicianName = "";
                }
            }
        }
        return view;
    }

    private void setSongsAdapter() {
        if (getActivity() != null && getActivity().getApplicationContext() != null && mSongs != null) {
            SongAdapter adapter = new SongAdapter(mSongs, getActivity().getApplicationContext());
            topSongsListView.setAdapter(adapter);
            topSongsListView.setOnItemClickListener((parent, v, position, id) -> sendToYoutube(position));
        }
    }

    private void sendToYoutube(int position) {
        Intent intent = new Intent(Intent.ACTION_SEARCH);
        intent.setPackage("com.google.android.youtube");
        intent.putExtra("query",  musicianName + " " + mSongs.get(position).getName());
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

}
