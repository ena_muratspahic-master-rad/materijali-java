package com.ena.muratspahic.materijali_java.model;

import android.os.Parcel;
import android.os.Parcelable;

public class Album implements Parcelable {

    private Integer id;
    private String spotifyId;
    private String name;
    private String spotifyLink;
    private String musicianId;

    public Album(String spotifyId, String name, String spotifyLink, String musicianId) {
        this.spotifyId = spotifyId;
        this.name = name;
        this.spotifyLink = spotifyLink;
        this.musicianId = musicianId;
    }

    public Album(Integer id, String spotifyId, String name, String spotifyLink, String musicianId) {
        this.id = id;
        this.spotifyId = spotifyId;
        this.name = name;
        this.spotifyLink = spotifyLink;
        this.musicianId = musicianId;
    }

    private Album(Parcel in) {
        if (in.readByte() == 0) {
            id = null;
        } else {
            id = in.readInt();
        }
        spotifyId = in.readString();
        name = in.readString();
        spotifyLink = in.readString();
        musicianId = in.readString();
    }

    public static final Creator<Album> CREATOR = new Creator<Album>() {
        @Override
        public Album createFromParcel(Parcel in) {
            return new Album(in);
        }

        @Override
        public Album[] newArray(int size) {
            return new Album[size];
        }
    };

    public Integer getId() { return id; }

    public void setId(Integer id) { this.id = id; }

    public String getSpotifyId() { return spotifyId;}

    public void setSpotifyId(String spotifyId) { this.spotifyId = spotifyId; }

    public String getName() { return name; }

    public void setName(String name) { this.name = name; }

    public String getSpotifyLink() { return spotifyLink; }

    public void setSpotifyLink(String spotifyLink) { this.spotifyLink = spotifyLink; }

    public String getMusicianId() { return musicianId; }

    public void setMusicianId(String musicianId) { this.musicianId = musicianId; }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        if (id == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(id);
        }
        dest.writeString(spotifyId);
        dest.writeString(name);
        dest.writeString(spotifyLink);
        dest.writeString(musicianId);
    }
}
