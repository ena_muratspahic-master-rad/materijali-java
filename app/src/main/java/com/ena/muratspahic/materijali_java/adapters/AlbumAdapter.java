package com.ena.muratspahic.materijali_java.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.ena.muratspahic.materijali_java.R;
import com.ena.muratspahic.materijali_java.model.Album;

import java.util.List;

public class AlbumAdapter extends ArrayAdapter<Album> {

    private static class AlbumViewHolder {
        TextView numberTextView;
        TextView nameTextView;
    }

    public AlbumAdapter(List<Album> data, Context context) {
        super(context, R.layout.list_element_numbered, data);
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, @NonNull ViewGroup parent) {
        Album album = getItem(position);

        AlbumViewHolder viewHolder;
        final View view;

        if (convertView == null) {

            viewHolder = new AlbumViewHolder();
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.list_element_numbered, parent, false);
            viewHolder.numberTextView = convertView.findViewById(R.id.numberTextView);
            viewHolder.nameTextView = convertView.findViewById(R.id.titleTextView);
            view = convertView;

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (AlbumViewHolder) convertView.getTag();
            view = convertView;
        }

        if (album != null) {
            String number = "%s.";
            viewHolder.numberTextView.setText(String.format(String.valueOf(position + 1), number));
            viewHolder.nameTextView.setText(album.getName());
        }

        return view;
    }

}