package com.ena.muratspahic.materijali_java.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.ena.muratspahic.materijali_java.R;
import com.ena.muratspahic.materijali_java.model.Musician;

import java.util.List;

public class MusicianAdapter extends RecyclerView.Adapter<MusicianAdapter.MusicianViewHolder> {

    private List<Musician> mData;
    private OnMusicianClickListener onMusicianClickListener;

    static class MusicianViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        ImageView genreImageView;
        TextView nameTextView;
        TextView genreTextView;
        OnMusicianClickListener onMusicianClickListener;

        MusicianViewHolder(@NonNull View view, OnMusicianClickListener onMusicianClickListener) {
            super(view);
            nameTextView = view.findViewById(R.id.nameTextView);
            genreTextView = view.findViewById(R.id.genre);
            genreImageView = view.findViewById(R.id.image);
            this.onMusicianClickListener = onMusicianClickListener;

            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            onMusicianClickListener.onMusicianClick(getAdapterPosition());
        }
    }

    public MusicianAdapter(List<Musician> data, OnMusicianClickListener onMusicianClickListener) {
        this.mData = data;
        this.onMusicianClickListener = onMusicianClickListener;
    }

    @NonNull
    @Override
    public MusicianViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        //Create new view
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_element, parent,false);
        return new MusicianViewHolder(view, onMusicianClickListener);
    }

    @Override
    public void onBindViewHolder(@NonNull MusicianViewHolder holder, int position) {
        Musician musician = mData.get(position);
        holder.genreImageView.setImageResource(musician.getGenreImageResource());
        holder.nameTextView.setText(musician.getName());
        holder.genreTextView.setText(musician.getGenre());

    }

    public interface OnMusicianClickListener {
        void onMusicianClick(int position);
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

}
