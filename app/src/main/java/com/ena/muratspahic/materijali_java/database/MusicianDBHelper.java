package com.ena.muratspahic.materijali_java.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.ena.muratspahic.materijali_java.model.Album;
import com.ena.muratspahic.materijali_java.model.Musician;
import com.ena.muratspahic.materijali_java.model.Search;
import com.ena.muratspahic.materijali_java.model.Song;

import java.util.ArrayList;
import java.util.List;

public class MusicianDBHelper extends SQLiteOpenHelper {

    public static final String DATABASE_NAME = "database.db";
    public static final int DATABASE_VERSION = 1;

    // MUSICIANS
    public static final String DATABASE_TABLE_MUSICIANS = "musicians";
    public static final String MUSICIAN_ID = "id";
    public static final String MUSICIAN_SPOTIFY_ID = "spotify_id";
    public static final String MUSICIAN_NAME = "name";
    public static final String MUSICIAN_GENRE = "genre";
    public static final String MUSICIAN_OFFICIAL_WEB = "official_web";
    public static final String MUSICIAN_IMAGE_URL = "image_url";

    // SONGS
    public static final String DATABASE_TABLE_SONGS = "songs";
    public static final String SONG_ID = "id";
    public static final String SONG_SPOTIFY_ID = "spotify_id";
    public static final String SONG_NAME = "name";
    public static final String SONG_MUSICIAN_ID = "musician_id";

    // ALBUMS
    public static final String DATABASE_TABLE_ALBUMS = "albums";
    public static final String ALBUM_ID = "id";
    public static final String ALBUM_SPOTIFY_ID = "spotify_id";
    public static final String ALBUM_NAME = "name";
    public static final String ALBUM_SPOTIFY_LINK = "spotify_link";
    public static final String ALBUM_MUSICIAN_ID = "musician_id";

    // SEARCH
    public static final String DATABASE_TABLE_SEARCH = "search";
    public static final String SEARCH_ID = "id";
    public static final String SEARCH_TEXT = "text";
    public static final String SEARCH_TIMESTAMP = "timestamp";
    public static final String SEARCH_STATUS = "status";

    // SQL upit za kreiranje baze
    private static final String DATABASE_MUSICIAN_CREATE = "CREATE TABLE " + DATABASE_TABLE_MUSICIANS
            + " (" + MUSICIAN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
            + MUSICIAN_SPOTIFY_ID + " text NOT NULL,"
            + MUSICIAN_NAME + " text NOT NULL,"
            + MUSICIAN_GENRE + " text NOT NULL,"
            + MUSICIAN_OFFICIAL_WEB + " text NOT NULL,"
            + MUSICIAN_IMAGE_URL + " text NOT NULL);";

    // SQL upit za kreiranje baze
    private static final String DATABASE_SONG_CREATE = "CREATE TABLE " + DATABASE_TABLE_SONGS
            + " (" + SONG_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
            + SONG_SPOTIFY_ID + " text NOT NULL,"
            + SONG_NAME + " text NOT NULL,"
            + SONG_MUSICIAN_ID + " text NOT NULL,"
            + " FOREIGN KEY (" + SONG_MUSICIAN_ID + ") REFERENCES " + DATABASE_TABLE_MUSICIANS + "(" + SONG_MUSICIAN_ID + "));";

    // SQL upit za kreiranje baze
    private static final String DATABASE_ALBUM_CREATE = "CREATE TABLE " + DATABASE_TABLE_ALBUMS
            + " (" + ALBUM_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
            + ALBUM_SPOTIFY_ID + " text NOT NULL,"
            + ALBUM_NAME + " text NOT NULL,"
            + ALBUM_SPOTIFY_LINK + " text NOT NULL,"
            + ALBUM_MUSICIAN_ID + " text NOT NULL,"
            + " FOREIGN KEY (" + ALBUM_MUSICIAN_ID + ") REFERENCES " + DATABASE_TABLE_MUSICIANS + "(" + ALBUM_MUSICIAN_ID + "));";

    // SQL upit za kreiranje baze
    private static final String DATABASE_SEARCH_CREATE = "CREATE TABLE " + DATABASE_TABLE_SEARCH
            + " (" + SEARCH_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
            + SEARCH_TEXT + " text NOT NULL,"
            + SEARCH_TIMESTAMP + " text NOT NULL,"
            + SEARCH_STATUS + " text NOT NULL);";


    public MusicianDBHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    public MusicianDBHelper(Context context, String databaseName, Object o, int databaseVersion) {
        super(context, databaseName, (SQLiteDatabase.CursorFactory) o, databaseVersion);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(DATABASE_MUSICIAN_CREATE);
        db.execSQL(DATABASE_SONG_CREATE);
        db.execSQL(DATABASE_ALBUM_CREATE);
        db.execSQL(DATABASE_SEARCH_CREATE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Brisanje stare verzije
        db.execSQL("DROP TABLE IF EXISTS " + DATABASE_TABLE_MUSICIANS);
        db.execSQL("DROP TABLE IF EXISTS " + DATABASE_TABLE_SONGS);
        db.execSQL("DROP TABLE IF EXISTS " + DATABASE_TABLE_ALBUMS);
        db.execSQL("DROP TABLE IF EXISTS " + DATABASE_TABLE_SEARCH);

        // Kreiranje nove
        onCreate(db);
    }

    // V6 - Zadatak 1
    public void deleteAll() {
        // Brisanje Svih podataka
        SQLiteDatabase db = this.getWritableDatabase();

        db.delete(DATABASE_TABLE_MUSICIANS,null,null);
        db.delete(DATABASE_TABLE_SONGS,null,null);
        db.delete(DATABASE_TABLE_ALBUMS,null,null);

        db.close();
    }

    // MUSICIAN
    public void addMusician(Musician musician) {
        if (!musicianExists(musician.getSpotifyId())) {
            SQLiteDatabase db = this.getWritableDatabase();

            ContentValues values = new ContentValues();
            values.put(MUSICIAN_SPOTIFY_ID, musician.getSpotifyId());
            values.put(MUSICIAN_NAME, musician.getName());
            values.put(MUSICIAN_GENRE, musician.getGenre());
            values.put(MUSICIAN_OFFICIAL_WEB, musician.getOfficialWebPage());
            values.put(MUSICIAN_IMAGE_URL, musician.getImageUrl());

            db.insert(DATABASE_TABLE_MUSICIANS, null, values);
            db.close();
        }
    }

    private boolean musicianExists(String spotifyId) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query(DATABASE_TABLE_MUSICIANS, new String[]{
                        MUSICIAN_ID, MUSICIAN_SPOTIFY_ID},
                MUSICIAN_SPOTIFY_ID + "=?",
                new String[]{String.valueOf(spotifyId)}, null, null, null, null);
        if (cursor != null && cursor.moveToNext()) {
            cursor.close();
            db.close();
            return true;
        }
        db.close();
        return false;
    }

    public Musician getMusician(String spotifyId) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query(DATABASE_TABLE_MUSICIANS, new String[]{
                        MUSICIAN_ID, MUSICIAN_SPOTIFY_ID, MUSICIAN_NAME, MUSICIAN_GENRE, MUSICIAN_OFFICIAL_WEB, MUSICIAN_IMAGE_URL},
                MUSICIAN_SPOTIFY_ID + "=?",
                new String[]{String.valueOf(spotifyId)}, null, null, null, null);
        Musician musician = new Musician();
        if (cursor != null && cursor.moveToNext()) {
            musician = new Musician(
                    Integer.parseInt(cursor.getString(0)),
                    cursor.getString(1),
                    cursor.getString(2),
                    cursor.getString(3),
                    cursor.getString(4),
                    cursor.getString(5));
            cursor.close();
        }
        db.close();

        return musician;
    }

    public String getMusicianName(String musicianId) {
        String artistName = "";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query(DATABASE_TABLE_MUSICIANS, new String[]{
                        MUSICIAN_SPOTIFY_ID, MUSICIAN_NAME},
                MUSICIAN_SPOTIFY_ID + "=?",
                new String[]{String.valueOf(musicianId)}, null, null, null, null);
        if (cursor != null && cursor.moveToNext()) {
            artistName =  cursor.getString(1);
            cursor.close();
        }
        db.close();

        return artistName;
    }

    public List<Musician> getAllMusicians() {
        List<Musician> musicians = new ArrayList<>();
        String SELECT_QUERY = "SELECT * FROM " + DATABASE_TABLE_MUSICIANS;

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(SELECT_QUERY, null);
        if (cursor.moveToFirst()) {
            do {
                Musician musician = new Musician();
                musician.setId(cursor.getInt(0));
                musician.setSpotifyId(cursor.getString(1));
                musician.setName(cursor.getString(2));
                musician.setGenre(cursor.getString(3));
                musician.setOfficialWebPage(cursor.getString(4));
                musician.setImageUrl(cursor.getString(5));

                musicians.add(musician);
            } while (cursor.moveToNext());
        }
        db.close();
        cursor.close();

        return musicians;
    }

    private void deleteMusician(Musician musician) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(DATABASE_TABLE_MUSICIANS, MUSICIAN_ID + "=?", new String[]{String.valueOf(musician.getId())});
        db.close();
    }

    private int getMusicianCount() {
        String countQuery = "SELECT * FROM " + DATABASE_TABLE_MUSICIANS;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);
        int count = cursor.getCount();
        cursor.close();

        return count;
    }

    // SONG
    private ContentValues getContentValueForSong(Song song) {
        ContentValues values = new ContentValues();
        values.put(SONG_SPOTIFY_ID, song.getSpotifyId());
        values.put(SONG_NAME, song.getName());
        values.put(SONG_MUSICIAN_ID, song.getMusicianId());

        return values;
    }

    public void addSongs(List<Song> songs) {
        SQLiteDatabase db = this.getWritableDatabase();

        for(int i = 0; i < songs.size(); i++) {
            ContentValues song = getContentValueForSong(songs.get(i));
            db.insert(DATABASE_TABLE_SONGS, null, song);
        }
        db.close();
    }

    private void deleteSong(Song song) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(DATABASE_TABLE_SONGS, SONG_ID + "=?", new String[]{String.valueOf(song.getId())});
        db.close();
    }

    public List<Song> getAllSongsFromMusician(String musicianId) {
        List<Song> songs = new ArrayList<>();

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query(DATABASE_TABLE_SONGS, new String[]{
                        SONG_ID, SONG_SPOTIFY_ID, SONG_NAME, SONG_MUSICIAN_ID},
                SONG_MUSICIAN_ID + "=?",
                new String[]{String.valueOf(musicianId)}, null, null, null, null);

        if (cursor != null && cursor.moveToFirst()) {
            do {
                Song song = new Song(cursor.getInt(0),
                        cursor.getString(1),
                        cursor.getString(2),
                        cursor.getString(3));

                songs.add(song);
            } while (cursor.moveToNext());
            cursor.close();
        }
        db.close();

        return songs;
    }

    public int getSongsCount() {
        String countQuery = "SELECT * FROM " + DATABASE_TABLE_SONGS;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);
        int count = cursor.getCount();
        cursor.close();

        return count;
    }


    // ALBUM
    private ContentValues getContentValuesForAlbum(Album album) {
        ContentValues values = new ContentValues();
        values.put(ALBUM_SPOTIFY_ID, album.getSpotifyId());
        values.put(ALBUM_NAME, album.getName());
        values.put(ALBUM_SPOTIFY_LINK, album.getSpotifyLink());
        values.put(ALBUM_MUSICIAN_ID, album.getMusicianId());

        return values;
    }

    public void addAlbums(List<Album> albums) {
        SQLiteDatabase db = this.getWritableDatabase();

        for(int i = 0; i < albums.size(); i++) {
            ContentValues album = getContentValuesForAlbum(albums.get(i));
            db.insert(DATABASE_TABLE_ALBUMS, null, album);
        }
        db.close();
    }

    private void deleteAlbum(Album album) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(DATABASE_TABLE_ALBUMS, ALBUM_ID + "=?", new String[]{String.valueOf(album.getId())});
        db.close();
    }


    public List<Album> getAllAlbumsFromMusician(String musicianId) {
        List<Album> albums = new ArrayList<>();

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query(DATABASE_TABLE_ALBUMS, new String[]{
                        ALBUM_ID, ALBUM_SPOTIFY_ID, ALBUM_NAME, ALBUM_SPOTIFY_LINK, ALBUM_MUSICIAN_ID},
                ALBUM_MUSICIAN_ID + "=?",
                new String[]{String.valueOf(musicianId)}, null, null, null, null);

        if (cursor != null && cursor.moveToFirst()) {
            do {
                Album album = new Album(cursor.getInt(0),
                        cursor.getString(1),
                        cursor.getString(2),
                        cursor.getString(3),
                        cursor.getString(4));
                albums.add(album);
            } while (cursor.moveToNext());
            cursor.close();
        }
        db.close();

        return albums;
    }

    public int getAlbumsCount() {
        String countQuery = "SELECT * FROM " + DATABASE_TABLE_ALBUMS;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);
        int count = cursor.getCount();
        cursor.close();

        return count;
    }

    public String getFirstMusicianSpotifyId() {
        String musicianSpotifyId = null;

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query(DATABASE_TABLE_MUSICIANS, new String[]{
                        MUSICIAN_ID, MUSICIAN_SPOTIFY_ID},
                MUSICIAN_ID + "=?",
                new String[]{"1"}, null, null, null, null);
        if (cursor != null && cursor.moveToNext()) {
            musicianSpotifyId = cursor.getString(1);
            cursor.close();
        }
        db.close();

        return musicianSpotifyId;
    }

    // SEARCH
    public void addSearch(Search search) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(SEARCH_TEXT, search.getSearchText());
        values.put(SEARCH_TIMESTAMP, search.getTimeStamp());
        values.put(SEARCH_STATUS, search.getStatus().toString());

        db.insert(DATABASE_TABLE_SEARCH, null, values);
        db.close();
    }

    public List<Search> getAllSearches() {
        List<Search> searches = new ArrayList<>();
        String SELECT_QUERY = "SELECT * FROM " + DATABASE_TABLE_SEARCH;

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(SELECT_QUERY, null);
        if (cursor.moveToFirst()) {
            do {
                Search search = new Search();
                search.setId(cursor.getInt(0));
                search.setSearchText(cursor.getString(1));
                search.setTimeStamp(cursor.getString(2));
                search.setStatus(cursor.getString(3).equals("true"));

                searches.add(search);
            } while (cursor.moveToNext());
        }
        cursor.close();
        db.close();

        return searches;
    }

    // DELETE FUNCTIONS

    public void deleteAllButLastFive() {
        List<Musician> musicians = getAllMusicians();
        for (int i = 0; i < getMusicianCount() - 5; i++) {
            Musician musician = musicians.get(i);
            deleteMusician(musician);
            deleteMusicianSongs(musician.getSpotifyId());
            deleteMusicianAlbums(musician.getSpotifyId());
        }
    }

    private void deleteMusicianSongs(String spotifyId) {
        List<Song> songs = getAllSongsFromMusician(spotifyId);
        for (int i = 0; i < songs.size(); i++) {
            Song song = songs.get(i);
            deleteSong(song);
        }
    }

    private void deleteMusicianAlbums(String spotifyId) {
        List<Album> albums = getAllAlbumsFromMusician(spotifyId);
        for (int i = 0; i < albums.size(); i++) {
            Album album = albums.get(i);
            deleteAlbum(album);
        }
    }

}
