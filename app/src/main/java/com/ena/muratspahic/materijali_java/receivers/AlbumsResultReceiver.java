package com.ena.muratspahic.materijali_java.receivers;

import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;

public class AlbumsResultReceiver extends ResultReceiver {

    private Receiver mReceiver;

    /**
     * Create a new ResultReceive to receive results.  Your
     * {@link #onReceiveResult} method will be called from the thread running
     * <var>handler</var> if given, or from an arbitrary thread if null.
     *
     */
    public AlbumsResultReceiver(Handler handler) {
        super(handler);
    }

    public void setReciever(Receiver reciever) {
        mReceiver = reciever;
    }

    public interface Receiver {
        void onReceiveAlbumsResult(int resultCode, Bundle resultData);
    }

    @Override
    protected void onReceiveResult(int resultCode, Bundle resultData) {
        if (mReceiver != null) {
            mReceiver.onReceiveAlbumsResult(resultCode, resultData);
        }
    }
}
