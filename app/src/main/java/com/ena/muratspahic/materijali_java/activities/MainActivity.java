package com.ena.muratspahic.materijali_java.activities;

import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.widget.FrameLayout;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;

import com.ena.muratspahic.materijali_java.R;
import com.ena.muratspahic.materijali_java.database.MusicianDBHelper;
import com.ena.muratspahic.materijali_java.fragments.FragmentMain;
import com.ena.muratspahic.materijali_java.fragments.FragmentMusician;
import com.ena.muratspahic.materijali_java.model.Musician;
import com.ena.muratspahic.materijali_java.receivers.InternetReceiver;
import com.ena.muratspahic.materijali_java.utils.Constants;

public class MainActivity extends AppCompatActivity implements FragmentMain.OnMusicianClick {

    boolean wide = false;

    private InternetReceiver internetReceiver = new InternetReceiver();

    // Database
    private MusicianDBHelper db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        // Database init
        db = new MusicianDBHelper(this);

        //Dohvatanje FragmentManager-a
        FragmentManager fragmentManager = getSupportFragmentManager();
        FrameLayout frameLayoutMusician = findViewById(R.id.fragmentTwo);

        if (frameLayoutMusician != null) {
            wide = true;

            FragmentMusician fragmentMusician = (FragmentMusician) fragmentManager.findFragmentById(R.id.fragmentTwo);
            if (fragmentMusician == null) {
                fragmentMusician = new FragmentMusician();
                String spotifyIdFirst = db.getFirstMusicianSpotifyId();
                if (spotifyIdFirst != null) {
                    Bundle arguments = new Bundle();
                    arguments.putString(Constants.MUSICIAN_SPOTIFY_ID, spotifyIdFirst);
                    fragmentMusician.setArguments(arguments);
                }
                fragmentManager.beginTransaction().replace(R.id.fragmentTwo, fragmentMusician).commit();
            }
        }

        FragmentMain fragmentMain = (FragmentMain) fragmentManager.findFragmentByTag("Main");
        if (fragmentMain == null) {
            fragmentMain = new FragmentMain();

            fragmentManager.beginTransaction().replace(R.id.fragmentOne, fragmentMain, "Main").commit();
        } else {
            fragmentManager.popBackStack(null,FragmentManager.POP_BACK_STACK_INCLUSIVE);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        registerReceiver(internetReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
    }

    @Override
    public void onPause() {
        super.onPause();
        unregisterReceiver(internetReceiver);
    }

    @Override
    public void onMusicianClicked(Musician musician) {
        // Add to database
        db.addMusician(musician);

        Bundle arguments = new Bundle();
        arguments.putString(Constants.MUSICIAN_SPOTIFY_ID, musician.getSpotifyId());
        FragmentMusician fragmentMusician = new FragmentMusician();
        fragmentMusician.setArguments(arguments);
        if (wide) {
            //Slučaj za ekrane sa širom dijagonalom
            getSupportFragmentManager().beginTransaction().replace(R.id.fragmentTwo, fragmentMusician)
                    .addToBackStack(null)
                    .commit();
        } else {
            getSupportFragmentManager().beginTransaction().replace(R.id.fragmentOne, fragmentMusician)
                    .addToBackStack(null)
                    .commit();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        db.deleteAllButLastFive();
    }

}

