package com.ena.muratspahic.materijali_java.services;

import android.app.IntentService;
import android.content.Intent;
import android.os.Bundle;
import android.os.ResultReceiver;

import androidx.annotation.Nullable;

import com.ena.muratspahic.materijali_java.model.Genre;
import com.ena.muratspahic.materijali_java.model.Musician;
import com.ena.muratspahic.materijali_java.receivers.SearchArtistResultReceiver;
import com.ena.muratspahic.materijali_java.utils.Constants;
import com.ena.muratspahic.materijali_java.utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;

public class SearchArtistIntentService extends IntentService {

    private ArrayList<Musician> musicians = new ArrayList<>();

    /**
     * Creates an IntentService.  Invoked by your subclass's constructor.
     *
     * @param name Used to name the worker thread, important only for debugging.
     */
    public SearchArtistIntentService(String name) {
        super(name);
    }

    public SearchArtistIntentService() {
        super(null);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        // Akcije koje se trebaju obaviti pri kreiranju servisa
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        // Kod koji se nalazi ovdje će se izvršavati u posebnoj niti ​
        // Ovdje treba da se nalazi funkcionalnost servisa koja je
        // vremenski zahtjevna
        if (intent != null) {
            String searchText = intent.getStringExtra(Constants.SERVICE_SEARCH_TEXT);
            final ResultReceiver receiver = intent.getParcelableExtra(Constants.SERVICE_RECEIVER);
            /* Update UI: Početak taska */
            receiver.send(SearchArtistResultReceiver.STATUS_RUNNING, Bundle.EMPTY);
            Bundle bundle = new Bundle();
            try {
                String query = URLEncoder.encode(searchText, "utf-8");
                String uri = "https://api.spotify.com/v1/search?q=" + query + "&type=artist&limit=10&offset=0";

                URL url = new URL(uri);
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                connection.setRequestProperty("Authorization", "Bearer " + Constants.token);

                InputStream inputStream = new BufferedInputStream(connection.getInputStream());
                String result = Utils.convertStreamToString(inputStream);
                JSONObject jsonObject = new JSONObject(result);
                JSONObject artists = jsonObject.getJSONObject("artists");
                JSONArray items = artists.getJSONArray("items");
                for(int i = 0; i < items.length(); i++) {
                    JSONObject artist = items.getJSONObject(i);

                    // If there is no genre then don't add this artist
                    if (artist.has("genres")) {

                        JSONArray genres = artist.getJSONArray("genres");
                        if (genres != null && genres.length() > 0) {
                            String id = artist.getString("id");
                            String name = artist.getString("name");
                            JSONObject externalUrls = artist.getJSONObject("external_urls");
                            String officialWebPage = externalUrls.getString("spotify");
                            String genre = genres.getString(0);
                            JSONArray images = artist.getJSONArray("images");
                            String imageUrl = Genre.getUnknownImageResource();
                            if (images != null) {
                                JSONObject firstImage = images.getJSONObject(0);
                                imageUrl = firstImage.getString("url");
                            }
                            if (!singerExists(name)) {
                                musicians.add(new Musician(id, name, genre, officialWebPage, imageUrl));
                            }
                        }
                    }
                }
                /* Proslijedi rezultate nazad u pozivatelja */
                bundle.putParcelableArrayList(Constants.SERVICE_MUSICIANS, musicians);
                receiver.send(SearchArtistResultReceiver.STATUS_FINISHED, bundle);
            } catch (IOException | JSONException exc) {
                exc.printStackTrace();
                /* Vrati obavijest da je došlo do izuzetka, e – izuzetak */
                bundle.putString(Intent.EXTRA_TEXT, String.valueOf(exc));
                receiver.send(SearchArtistResultReceiver.STATUS_ERROR, bundle);
            }
        }
    }

    private boolean singerExists(String name) {
        for (Musician musician: musicians) {
            if (musician.getName().equals(name))
                return true;
        }
        return false;
    }
}
