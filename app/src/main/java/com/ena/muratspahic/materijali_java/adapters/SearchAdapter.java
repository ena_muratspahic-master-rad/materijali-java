package com.ena.muratspahic.materijali_java.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.ena.muratspahic.materijali_java.R;
import com.ena.muratspahic.materijali_java.model.Search;

import java.util.List;

public class SearchAdapter extends RecyclerView.Adapter<SearchAdapter.SearchViewHolder> {

    private List<Search> mData;
    private OnSearchClickListener onSearchClickListener;

    static class SearchViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView searchTextView;
        TextView timestampTextView;
        ConstraintLayout searchContainer;
        OnSearchClickListener onSearchClickListener;

        SearchViewHolder(@NonNull View view, OnSearchClickListener onSearchClickListener) {
            super(view);
            searchTextView = view.findViewById(R.id.searchedTextView);
            timestampTextView = view.findViewById(R.id.timeTextView);
            searchContainer = view.findViewById(R.id.container_search);
            this.onSearchClickListener = onSearchClickListener;

            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            onSearchClickListener.onSearchClick(getAdapterPosition());
        }
    }

    public SearchAdapter(List<Search> data, OnSearchClickListener onSearchClickListener) {
        this.mData = data;
        this.onSearchClickListener = onSearchClickListener;
    }

    @NonNull
    @Override
    public SearchViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        //Create new view
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_searched_element, parent,false);
        return new SearchViewHolder(view, onSearchClickListener);
    }

    @Override
    public void onBindViewHolder(@NonNull SearchViewHolder holder, int position) {
        Search search = mData.get(position);
        holder.searchTextView.setText(search.getSearchText());
        holder.timestampTextView.setText(search.getTimeStamp());

        int statusColor = search.getStatus() ? R.color.search_success : R.color.search_fail;

        holder.searchContainer.setBackgroundColor(ContextCompat.getColor(holder.itemView.getContext(), statusColor));
    }

    public interface OnSearchClickListener {
        void onSearchClick(int position);
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

}

