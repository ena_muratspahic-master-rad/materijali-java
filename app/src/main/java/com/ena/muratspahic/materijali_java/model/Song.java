package com.ena.muratspahic.materijali_java.model;

import android.os.Parcel;
import android.os.Parcelable;

public class Song implements Parcelable {

    private Integer id;
    private String spotifyId;
    private String name;
    private String musicianId;

    public Song(String spotifyId, String name, String musicianId) {
        this.spotifyId = spotifyId;
        this.name = name;
        this.musicianId = musicianId;
    }

    public Song(Integer id, String spotifyId, String name, String musicianId) {
        this.id = id;
        this.spotifyId = spotifyId;
        this.name = name;
        this.musicianId = musicianId;
    }

    private Song(Parcel in) {
        if (in.readByte() == 0) {
            id = null;
        } else {
            id = in.readInt();
        }
        spotifyId = in.readString();
        name = in.readString();
        musicianId = in.readString();
    }

    public static final Creator<Song> CREATOR = new Creator<Song>() {
        @Override
        public Song createFromParcel(Parcel in) {
            return new Song(in);
        }

        @Override
        public Song[] newArray(int size) {
            return new Song[size];
        }
    };

    public Integer getId() { return id; }

    public void setId(Integer id) { this.id = id; }

    public String getSpotifyId() { return spotifyId; }

    public void setSpotifyId(String spotifyId) { this.spotifyId = spotifyId; }

    public String getName() {
        return name;
    }

    public void setName(String name) { this.name = name; }

    public String getMusicianId() { return musicianId; }

    public void setMusicianId(String musicianId) { this.musicianId = musicianId; }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        if (id == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(id);
        }
        dest.writeString(spotifyId);
        dest.writeString(name);
        dest.writeString(musicianId);
    }
}
