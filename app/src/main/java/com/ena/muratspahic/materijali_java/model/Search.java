package com.ena.muratspahic.materijali_java.model;

public class Search {

    private Integer id;
    private String searchText;
    private String timeStamp;
    private Boolean status;

    public Search() { }

    public Search(String searchText, String timeStamp, Boolean status) {
        this.searchText = searchText;
        this.timeStamp = timeStamp;
        this.status = status;
    }

    public Integer getId() { return id; }

    public void setId(Integer id) { this.id = id; }

    public String getSearchText() { return searchText; }

    public void setSearchText(String searchText) { this.searchText = searchText; }

    public String getTimeStamp() { return timeStamp; }

    public void setTimeStamp(String timeStamp) { this.timeStamp = timeStamp; }

    public Boolean getStatus() { return status; }

    public void setStatus(Boolean status) { this.status = status; }

}

