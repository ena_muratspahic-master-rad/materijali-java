package com.ena.muratspahic.materijali_java.fragments;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.ena.muratspahic.materijali_java.R;
import com.ena.muratspahic.materijali_java.database.MusicianDBHelper;
import com.ena.muratspahic.materijali_java.dialogs.CustomDialog;
import com.ena.muratspahic.materijali_java.model.Album;
import com.ena.muratspahic.materijali_java.model.Musician;
import com.ena.muratspahic.materijali_java.model.Song;
import com.ena.muratspahic.materijali_java.receivers.AlbumsResultReceiver;
import com.ena.muratspahic.materijali_java.receivers.TopSongsResultReceiver;
import com.ena.muratspahic.materijali_java.services.AlbumsIntentService;
import com.ena.muratspahic.materijali_java.services.TopSongsIntentService;
import com.ena.muratspahic.materijali_java.utils.Constants;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Objects;

public class FragmentMusician extends Fragment implements TopSongsResultReceiver.Receiver, AlbumsResultReceiver.Receiver {

    private boolean songViewIsDisplayed = false;

    private FrameLayout frameLayoutTopFive;

    private Musician mMusician = new Musician();

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_musician,container,false);

        if (getArguments() != null && getArguments().containsKey(Constants.MUSICIAN_SPOTIFY_ID)) {
            String musicianId = getArguments().getString(Constants.MUSICIAN_SPOTIFY_ID);

            try (MusicianDBHelper db = new MusicianDBHelper(getActivity())) {
                mMusician = db.getMusician(musicianId);

                ImageView musicianImageView = view.findViewById(R.id.genreImageView);

                //Loading image using Picasso
                Picasso.get().load(mMusician.getImageUrl()).into(musicianImageView);

                TextView nameTextView = view.findViewById(R.id.nameTextView);
                nameTextView.setText(mMusician.getName());

                TextView officialLinkTextView = view.findViewById(R.id.officialLinkTextView);
                officialLinkTextView.setText(Html.fromHtml("<a href=\""+ mMusician.getOfficialWebPage() + "\">" + getString(R.string.open_artist) + "</a>"));
                officialLinkTextView.setOnClickListener(v -> openOfficialPage());

                TextView genreTextView = view.findViewById(R.id.genreTextView);
                genreTextView.setText(mMusician.getGenre());

                Button shareButton = view.findViewById(R.id.shareButton);
                shareButton.setOnClickListener(v -> sendInfoToOtherApp());

                Button changeViewButton = view.findViewById(R.id.changeViewButton);
                changeViewButton.setOnClickListener(v -> {
                    // Change text on the button and change view
                    changeViewButton.setText(songViewIsDisplayed ? getString(R.string.top_songs) : getString(R.string.albums));
                    changeView();
                });

                frameLayoutTopFive = view.findViewById(R.id.fragmentChild);

                searchMusicianTopSongs(mMusician.getSpotifyId());
                searchMusicianAlbums(mMusician.getSpotifyId());

            } finally {
                mMusician = null;
            }

        } else {
            ImageView musicianImageView = view.findViewById(R.id.genreImageView);
            musicianImageView.setVisibility(View.GONE);
            Button shareButton = view.findViewById(R.id.shareButton);
            shareButton.setVisibility(View.GONE);
            Button changeViewButton = view.findViewById(R.id.changeViewButton);
            changeViewButton.setVisibility(View.GONE);
        }
        return view;
    }

    private void searchMusicianTopSongs(String spotifyId) {
        Intent topSongsIntent = new Intent(Intent.ACTION_SYNC, null, getActivity(), TopSongsIntentService.class);
        topSongsIntent.putExtra(Constants.SERVICE_MUSICIAN_ID, spotifyId);
        //Result Receiver
        TopSongsResultReceiver mReceiver = new TopSongsResultReceiver(new Handler());
        mReceiver.setReciever(this);
        topSongsIntent.putExtra(Constants.SERVICE_RECEIVER, mReceiver);
        Objects.requireNonNull(getActivity()).startService(topSongsIntent);
    }


    private void searchMusicianAlbums(String spotifyId) {
        Intent albumsIntent = new Intent(Intent.ACTION_SYNC, null, getActivity(), AlbumsIntentService.class);
        albumsIntent.putExtra(Constants.SERVICE_MUSICIAN_ID, spotifyId);
        //Result Receiver
        AlbumsResultReceiver mReceiver = new AlbumsResultReceiver(new Handler());
        mReceiver.setReciever(this);
        albumsIntent.putExtra(Constants.SERVICE_RECEIVER, mReceiver);
        Objects.requireNonNull(getActivity()).startService(albumsIntent);
    }

    private void changeView() {
        Bundle arguments = new Bundle();
        arguments.putString(Constants.MUSICIAN_SPOTIFY_ID, mMusician.getSpotifyId());
        if (!songViewIsDisplayed) {
            setSongsView(arguments);
        } else {
            setSimilarView(arguments);
        }
        songViewIsDisplayed = !songViewIsDisplayed;
    }

    private void setSongsView(Bundle arguments) {
        FragmentManager fragmentManager = getChildFragmentManager();
        if (frameLayoutTopFive != null) {
            ChildFragmentTopSongs childFragmentTopSongs = new ChildFragmentTopSongs();
            FragmentTransaction transaction = fragmentManager.beginTransaction();

            childFragmentTopSongs.setArguments(arguments);
            transaction.replace(R.id.fragmentChild, childFragmentTopSongs);
            transaction.commit();
        }
    }

    private void setSimilarView(Bundle arguments) {
        FragmentManager fragmentManager = getChildFragmentManager();
        if (frameLayoutTopFive != null) {
            ChildFragmentAlbums childFragmentAlbums = new ChildFragmentAlbums();
            FragmentTransaction transaction = fragmentManager.beginTransaction();

            childFragmentAlbums.setArguments(arguments);
            transaction.replace(R.id.fragmentChild, childFragmentAlbums);
            transaction.commit();
        }
    }

    private void openOfficialPage() {
        Intent openOfficialPage = new Intent(Intent.ACTION_VIEW);
        openOfficialPage.setData(Uri.parse(mMusician.getOfficialWebPage()));
        startActivity(openOfficialPage);
    }

    private void sendInfoToOtherApp() {
        Intent sendEmailIntent = new Intent(Intent.ACTION_SEND);
        sendEmailIntent.setType("text/plain");
        sendEmailIntent.putExtra(Intent.EXTRA_EMAIL, new String[] {Constants.recipientForSendEmailIntent});
        sendEmailIntent.putExtra(Intent.EXTRA_SUBJECT, mMusician.getName());
        sendEmailIntent.putExtra(Intent.EXTRA_TEXT, mMusician.getName() + " " + mMusician.getOfficialWebPage());

        if (getActivity() != null) {
            if (sendEmailIntent.resolveActivity(getActivity().getPackageManager()) != null) {
                startActivity(sendEmailIntent);
            }
        }
    }

    @Override
    public void onReceiveSongsResult(int resultCode, Bundle resultData) {
        switch (resultCode) {
            case 0: // Call upucen
                //Add something
                break;
            case 1:// Fetch result
                ArrayList<Song> songs = resultData.getParcelableArrayList(Constants.SERVICE_SONGS);
                if (songs != null) {
                    // Add to database
                    try (MusicianDBHelper db = new MusicianDBHelper(getActivity())) {
                        db.addSongs(songs);
                        changeView();
                    }
                }
                break;
            case 2:// Error
                String error = resultData.getString(Intent.EXTRA_TEXT);
                Log.e(getString(R.string.error), error);
                showCustomDialog(getString(R.string.error), getString(R.string.error_text), getString(R.string.internet_button));
                break;
            default:
                throw new IllegalStateException("Unexpected value: " + resultCode);
        }
    }

    @Override
    public void onReceiveAlbumsResult(int resultCode, Bundle resultData) {
        switch (resultCode) {
            case 0: // Call upucen
                //Add something
                break;
            case 1:// Fetch result
                ArrayList<Album> albums = resultData.getParcelableArrayList(Constants.SERVICE_ALBUMS);
                if (albums != null) {
                    // Add to database
                    try (MusicianDBHelper db = new MusicianDBHelper(getActivity())) {
                        db.addAlbums(albums);
                    }
                }
                break;
            case 2:// Error
                String error = resultData.getString(Intent.EXTRA_TEXT);
                Log.e(getString(R.string.error), error);
                showCustomDialog(getString(R.string.error), getString(R.string.error_text), getString(R.string.internet_button));
                break;
            default:
                throw new IllegalStateException("Unexpected value: " + resultCode);
        }
    }

    private void showCustomDialog(String titleText, String messageText, String buttonText) {
        if (getContext() != null) {
            CustomDialog dialog = new CustomDialog(getContext(), titleText, messageText, buttonText);

            WindowManager.LayoutParams layoutParams = new WindowManager.LayoutParams();
            layoutParams.copyFrom(Objects.requireNonNull(dialog.getWindow()).getAttributes());
            layoutParams.width = WindowManager.LayoutParams.MATCH_PARENT;
            layoutParams.height = WindowManager.LayoutParams.WRAP_CONTENT;

            dialog.show();
            dialog.getWindow().setAttributes(layoutParams);
        }
    }
}
