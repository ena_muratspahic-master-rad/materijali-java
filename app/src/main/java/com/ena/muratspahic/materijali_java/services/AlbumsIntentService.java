package com.ena.muratspahic.materijali_java.services;

import android.app.IntentService;
import android.content.Intent;
import android.os.Bundle;
import android.os.ResultReceiver;

import androidx.annotation.Nullable;

import com.ena.muratspahic.materijali_java.model.Album;
import com.ena.muratspahic.materijali_java.receivers.TopSongsResultReceiver;
import com.ena.muratspahic.materijali_java.utils.Constants;
import com.ena.muratspahic.materijali_java.utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;

public class AlbumsIntentService extends IntentService {

    private ArrayList<Album> albums = new ArrayList<>();

    /**
     * Creates an IntentService.  Invoked by your subclass's constructor.
     *
     * @param name Used to name the worker thread, important only for debugging.
     */
    public AlbumsIntentService(String name) {
        super(name);
    }

    public AlbumsIntentService() {
        super(null);
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        if (intent != null) {
            String musicianId = intent.getStringExtra(Constants.SERVICE_MUSICIAN_ID);
            final ResultReceiver receiver = intent.getParcelableExtra(Constants.SERVICE_RECEIVER);
            /* Update UI: Početak taska */
            receiver.send(TopSongsResultReceiver.STATUS_RUNNING, Bundle.EMPTY);
            Bundle bundle = new Bundle();
            try {
                String query = URLEncoder.encode(musicianId, "utf-8");
                String uri = "https://api.spotify.com/v1/artists/" + query + "/albums";

                URL url = new URL(uri);
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                connection.setRequestProperty("Authorization", "Bearer " + Constants.token);

                InputStream inputStream = new BufferedInputStream(connection.getInputStream());
                String result = Utils.convertStreamToString(inputStream);
                JSONObject jsonObject = new JSONObject(result);
                JSONArray items = jsonObject.getJSONArray("items");
                for (int i = 0; i < items.length(); i++) {
                    JSONObject song = items.getJSONObject(i);

                    String albumName = song.getString("name");
                    if (!albumExists(albumName)) {
                        String id = song.getString("id");

                        JSONObject externalUrls = song.getJSONObject("external_urls");
                        String spotifyLink = externalUrls.getString("spotify");

                        albums.add(new Album(id, albumName, spotifyLink, musicianId));
                    }
                }

                /* Proslijedi rezultate nazad u pozivatelja */
                bundle.putParcelableArrayList(Constants.SERVICE_ALBUMS, albums);
                receiver.send(TopSongsResultReceiver.STATUS_FINISHED, bundle);
            } catch (IOException | JSONException exc) {
                exc.printStackTrace();
                /* Vrati obavijest da je došlo do izuzetka, e – izuzetak */
                bundle.putString(Intent.EXTRA_TEXT, String.valueOf(exc));
                receiver.send(TopSongsResultReceiver.STATUS_ERROR, bundle);
            }
        }
    }

    private boolean albumExists(String albumName) {
        for (Album album: albums) {
            if (album.getName().equals(albumName))
                return true;
        }
        return false;
    }

}
