package com.ena.muratspahic.materijali_java.model;

import android.os.Parcel;
import android.os.Parcelable;

public class Musician implements Parcelable {

    private Integer id;
    private String spotifyId;
    private String name;
    private String genre;
    private String officialWebPage;
    private String imageUrl;

    public Musician() { }

    public Musician(Integer id, String spotifyId, String name, String genre, String officialWebPage, String imageUrl) {
        this.id = id;
        this.spotifyId = spotifyId;
        this.name = name;
        this.genre = genre;
        this.officialWebPage = officialWebPage;
        this.imageUrl = imageUrl;
    }

    public Musician(String spotifyId, String name, String genre, String officialWebPage, String imageUrl) {
        this.spotifyId = spotifyId;
        this.name = name;
        this.genre = genre;
        this.officialWebPage = officialWebPage;
        this.imageUrl = imageUrl;
    }

    private Musician(Parcel in) {
        if (in.readByte() == 0) {
            id = null;
        } else {
            id = in.readInt();
        }
        spotifyId = in.readString();
        name = in.readString();
        genre = in.readString();
        officialWebPage = in.readString();
        imageUrl = in.readString();
    }

    public static final Creator<Musician> CREATOR = new Creator<Musician>() {
        @Override
        public Musician createFromParcel(Parcel in) {
            return new Musician(in);
        }

        @Override
        public Musician[] newArray(int size) {
            return new Musician[size];
        }
    };


    public Integer getId() { return id; }

    public void setId(Integer id) { this.id = id; }

    public String getSpotifyId() { return spotifyId; }

    public void setSpotifyId(String spotifyId) { this.spotifyId = spotifyId; }

    public String getName() { return name; }

    public void setName(String name) {
        this.name = name;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public String getOfficialWebPage() {
        return officialWebPage;
    }

    public void setOfficialWebPage(String officialWebPage) { this.officialWebPage = officialWebPage; }

    public String getImageUrl() { return imageUrl; }

    public void setImageUrl(String imageUrl) { this.imageUrl = imageUrl; }

    public int getGenreImageResource() { return Genre.getImageResource(getGenre()); }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        if (id == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(id);
        }
        dest.writeString(spotifyId);
        dest.writeString(name);
        dest.writeString(genre);
        dest.writeString(officialWebPage);
        dest.writeString(imageUrl);
    }
}
