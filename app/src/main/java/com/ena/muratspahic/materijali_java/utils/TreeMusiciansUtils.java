package com.ena.muratspahic.materijali_java.utils;

import com.ena.muratspahic.materijali_java.model.Album;
import com.ena.muratspahic.materijali_java.model.Musician;
import com.ena.muratspahic.materijali_java.model.Song;

import java.util.ArrayList;
import java.util.List;

// V6 - Zadatak 1
public class TreeMusiciansUtils {

    // Musician(Integer id, String spotifyId, String name, String genre, String officialWebPage, String imageUrl) {
    public static List<Musician> getMusicians() {
        List<Musician> musicians = new ArrayList<>();
        musicians.add(new Musician("0NIIxcxNHmOoyBx03SfTCD","Tinashe", "electropop","https://open.spotify.com/artist/0NIIxcxNHmOoyBx03SfTCD","https://i.scdn.co/image/d3dce253bfa66403e9cb2a3aa4a6d33775cd43c7"));
        musicians.add(new Musician("1HY2Jd0NmPuamShAr6KMms","Lady Gaga", "dance pop","https://open.spotify.com/artist/1HY2Jd0NmPuamShAr6KMms","https://i.scdn.co/image/ea76c82c05174105751f850c8a1db426dd03aa78"));
        musicians.add(new Musician("6ueGR6SWhUJfvEhqkvMsVs","Janelle Monáe", "alternative r&b","https://open.spotify.com/artist/6ueGR6SWhUJfvEhqkvMsVs","https://i.scdn.co/image/01725ba396132ac8166d89d5c5d9b4afc3b83229"));

        return musicians;
    }

    // Song(Integer id, String spotifyId, String name, String musicianId)
    public static ArrayList<Song> getSongs() {
        ArrayList<Song> songs = new ArrayList<>();
        // Tinashe
        songs.add(new Song("3jVtllWS5CFFWLQng8sKsr", "2 On (feat. ScHoolboy Q)","0NIIxcxNHmOoyBx03SfTCD"));
        songs.add(new Song("7D8aQaRzoi9Qzz5yerVK5b", "Throw A Fit","0NIIxcxNHmOoyBx03SfTCD"));
        songs.add(new Song("4cOVTA2GfYTHw99AJDQpHo", "The Worst In Me","0NIIxcxNHmOoyBx03SfTCD"));
        songs.add(new Song("3fMKnwiByu9yfeD5ISn9Et", "Like I Used To","0NIIxcxNHmOoyBx03SfTCD"));
        songs.add(new Song("3TMZVleKFb75oUZpp86KPn", "Save Room for Us","0NIIxcxNHmOoyBx03SfTCD"));
        songs.add(new Song("0qCLPgY9yoRdDrwoF73Hnq", "Link Up","0NIIxcxNHmOoyBx03SfTCD"));
        songs.add(new Song("0MoGkelRvi3gxZy6kuYHLq", "Touch & Go","0NIIxcxNHmOoyBx03SfTCD"));
        songs.add(new Song("2f9FpXmDVxZt7j1mh7Lc2X", "Hopscotch","0NIIxcxNHmOoyBx03SfTCD"));
        songs.add(new Song("1uHACkBD8IHBmrkTrk6You", "up in this (with Tinashe)","0NIIxcxNHmOoyBx03SfTCD"));
        songs.add(new Song("5ImmilkfIH2kTLYnGaqOqO", "Feelings","0NIIxcxNHmOoyBx03SfTCD"));

        // Lady Gaga
        songs.add(new Song("2VxeLyX666F8uXCJ0dZF8B", "Shallow","1HY2Jd0NmPuamShAr6KMms"));
        songs.add(new Song("2rbDhOo9Fh61Bbu23T2qCk", "Always Remember Us This Way","1HY2Jd0NmPuamShAr6KMms"));
        songs.add(new Song("0SiywuOBRcynK0uKGWdCnn", "Bad Romance","1HY2Jd0NmPuamShAr6KMms"));
        songs.add(new Song("7dZ1Odmx9jWIweQSatnRqo", "Million Reasons","1HY2Jd0NmPuamShAr6KMms"));
        songs.add(new Song("0WfKDYeUAoLA3vdvLKKWMW", "Poker Face","1HY2Jd0NmPuamShAr6KMms"));
        songs.add(new Song("6HkjpGIOHv4y0IBAxUbE5z", "Just Dance","1HY2Jd0NmPuamShAr6KMms"));
        songs.add(new Song("3DKpA54hrFIdPN6AtL9HXa", "I'll Never Love Again - Film Version - Radio Edit","1HY2Jd0NmPuamShAr6KMms"));
        songs.add(new Song("7rl7ao5pb9BhvAzPdWStxi", "Telephone","1HY2Jd0NmPuamShAr6KMms"));
        songs.add(new Song("6r2BECwMgEoRb5yLfp0Hca", "Born This Way","1HY2Jd0NmPuamShAr6KMms"));
        songs.add(new Song("5ka2ajep9OAvU5Sgduhiex", "Applause","1HY2Jd0NmPuamShAr6KMms"));

        // Janealle Monae
        songs.add(new Song("5gW5dSy3vXJxgzma4rQuzH", "Make Me Feel","6ueGR6SWhUJfvEhqkvMsVs"));
        songs.add(new Song("2EznBGrlmx9wBeYgyDojsA", "I Like That","6ueGR6SWhUJfvEhqkvMsVs"));
        songs.add(new Song("5OpiyfqaQLdtwHd3SfembH", "Pynk (feat. Grimes)","6ueGR6SWhUJfvEhqkvMsVs"));
        songs.add(new Song("3JI2mIJto0JuYbrq87aFqu", "Venus Fly","6ueGR6SWhUJfvEhqkvMsVs"));
        songs.add(new Song("1ljzHUgt2SU2ADkhfa9eBC", "Tightrope (feat. Big Boi) - Big Boi Vocal Edit","6ueGR6SWhUJfvEhqkvMsVs"));
        songs.add(new Song("3IJCSQoLF4YzPAKaxq2JLb", "Yoga","6ueGR6SWhUJfvEhqkvMsVs"));
        songs.add(new Song("1Z2MfAx1nJ09NzGjodnvRW", "Screwed (feat. Zoë Kravitz)","6ueGR6SWhUJfvEhqkvMsVs"));
        songs.add(new Song("56RfNBJGUgL1ZFCB1KEJrQ", "Django Jane","6ueGR6SWhUJfvEhqkvMsVs"));
        songs.add(new Song("3HW030T8eqPs8wpsgZqCGM", "Q.U.E.E.N. (feat. Erykah Badu)","6ueGR6SWhUJfvEhqkvMsVs"));
        songs.add(new Song("5dzV75f9qVXVvdXLTqIG4L", "Primetime (feat. Miguel)","6ueGR6SWhUJfvEhqkvMsVs"));

        return songs;
    }

    // Album(Integer id, String spotifyId, String name, String spotifyLink, String musicianId)
    public static ArrayList<Album> getAlbums() {
        ArrayList<Album> albums = new ArrayList<>();
         // Tinashe
        albums.add(new Album("03mqFnIf0SxlMFVDO4a3YK","Songs For You","https://open.spotify.com/album/03mqFnIf0SxlMFVDO4a3YK","0NIIxcxNHmOoyBx03SfTCD"));
        albums.add(new Album("78awGdD8XzM86ZCvWRLR4y","Joyride (Japan Version)","https://open.spotify.com/album/78awGdD8XzM86ZCvWRLR4y","0NIIxcxNHmOoyBx03SfTCD"));
        albums.add(new Album("0u3rjsCgagcSxHoRtXMKQo","Joyride","https://open.spotify.com/album/0u3rjsCgagcSxHoRtXMKQo","0NIIxcxNHmOoyBx03SfTCD"));
        albums.add(new Album("4oUQWJAcEtTuhYFR9AGfaC","Nightride","https://open.spotify.com/album/4oUQWJAcEtTuhYFR9AGfaC","0NIIxcxNHmOoyBx03SfTCD"));
        albums.add(new Album("0kAjVhWydfRkTlG95XWHOB","Aquarius","https://open.spotify.com/album/0kAjVhWydfRkTlG95XWHOB","0NIIxcxNHmOoyBx03SfTCD"));

        // Lady Gaga
        albums.add(new Album("4sLtOBOzn4s3GDUv3c5oJD","A Star Is Born Soundtrack","https://open.spotify.com/album/4sLtOBOzn4s3GDUv3c5oJD","1HY2Jd0NmPuamShAr6KMms"));
        albums.add(new Album("3edjzMAVB9RYRd4UcZBchx","A Star Is Born Soundtrack (Without Dialogue)","https://open.spotify.com/album/3edjzMAVB9RYRd4UcZBchx","1HY2Jd0NmPuamShAr6KMms"));
        albums.add(new Album("0akg3PQJSnnYsFSS0Qmizu","Joanne","https://open.spotify.com/album/0akg3PQJSnnYsFSS0Qmizu","1HY2Jd0NmPuamShAr6KMms"));
        albums.add(new Album("2ZUwFxlWo0gwTsvZ6L4Meh","Joanne (Deluxe)","https://open.spotify.com/album/2ZUwFxlWo0gwTsvZ6L4Meh","1HY2Jd0NmPuamShAr6KMms"));
        albums.add(new Album("1nCIPxGvDnphvLE1dYm5zU","Cheek To Cheek (Deluxe)","https://open.spotify.com/album/1nCIPxGvDnphvLE1dYm5zU","1HY2Jd0NmPuamShAr6KMms"));

        // Janealle monae
        albums.add(new Album("2PjlaxlMunGOUvcRzlTbtE","Dirty Computer","https://open.spotify.com/album/2PjlaxlMunGOUvcRzlTbtE","6ueGR6SWhUJfvEhqkvMsVs"));
        albums.add(new Album("3bnHtSmmsgJiG82hGCmsq9","The Electric Lady","https://open.spotify.com/album/3bnHtSmmsgJiG82hGCmsq9","6ueGR6SWhUJfvEhqkvMsVs"));
        albums.add(new Album("6NwW9scjPK0lqDhnP0qust","The ArchAndroid (Tour Edition)","https://open.spotify.com/album/6NwW9scjPK0lqDhnP0qust","6ueGR6SWhUJfvEhqkvMsVs"));
        albums.add(new Album("7MvSB0JTdtl1pSwZcgvYQX","The ArchAndroid","https://open.spotify.com/album/7MvSB0JTdtl1pSwZcgvYQX","6ueGR6SWhUJfvEhqkvMsVs"));
        albums.add(new Album("3T3bJi3cvwR5U7ihwgEwF1","Metropolis: The Chase Suite (Special Edition)","https://open.spotify.com/album/3T3bJi3cvwR5U7ihwgEwF1","6ueGR6SWhUJfvEhqkvMsVs"));
        return albums;
    }
}
