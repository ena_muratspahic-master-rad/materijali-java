package com.ena.muratspahic.materijali_java.fragments;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.ena.muratspahic.materijali_java.R;
import com.ena.muratspahic.materijali_java.adapters.MusicianAdapter;
import com.ena.muratspahic.materijali_java.adapters.SearchAdapter;
import com.ena.muratspahic.materijali_java.database.MusicianDBHelper;
import com.ena.muratspahic.materijali_java.dialogs.CustomDialog;
import com.ena.muratspahic.materijali_java.model.Musician;
import com.ena.muratspahic.materijali_java.model.Search;
import com.ena.muratspahic.materijali_java.receivers.SearchArtistResultReceiver;
import com.ena.muratspahic.materijali_java.services.SearchArtistIntentService;
import com.ena.muratspahic.materijali_java.utils.Constants;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

public class FragmentMain extends Fragment implements MusicianAdapter.OnMusicianClickListener, SearchAdapter.OnSearchClickListener, SearchArtistResultReceiver.Receiver {

    private MusicianDBHelper db;

    private List<Musician> mData = new ArrayList<>();
    private OnMusicianClick onMusicianClick;

    private List<Search> mSearches = new ArrayList<>();

    private EditText searchEditText;
    private RecyclerView recyclerView;
    private Button searchButton;

    private String searchedText;

    private boolean searchesDisplayed = false;

    private Button changeListButton;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_main,container,false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        if (getView() != null) {
            // Casting
            searchEditText = getView().findViewById(R.id.searchEditText);

            searchButton = getView().findViewById(R.id.searchButton);
            searchButton.setOnClickListener(v -> {
                this.searchedText = searchEditText.getText().toString();
                search();
            });

            changeListButton = getView().findViewById(R.id.changeListButton);
            changeListButton.setOnClickListener(v -> changeView());

            recyclerView = getView().findViewById(R.id.listView);
            recyclerView.setHasFixedSize(true);

            db = new MusicianDBHelper(getActivity());
            mData = db.getAllMusicians();
            mSearches = db.getAllSearches();

            setMusicianAdapterData();
        }

        try {
            onMusicianClick = (OnMusicianClick)getActivity();
        } catch (ClassCastException e) {
            throw new ClassCastException(getActivity() + "Implement OnMusicianClick");
        }

        if (getActivity()!= null && getActivity().getIntent() != null) {
            Intent intent = getActivity().getIntent();
            String type = intent.getType();
            if (Intent.ACTION_SEND.equals(intent.getAction()) && type != null) {
                if ("text/plain".equals(type)) {
                    handleSendText(intent);
                }
            }
        }
    }

    private void search() {
        if(connectedToInternet()) {
            runArtistSearch();
        } else {
            // Add to listOfSearches
            addToListOfSearches(new Search(searchedText, getTime(), false));
            if (searchesDisplayed) {
                setSearchesAdapterData();
            }
            showCustomDialog(getString(R.string.internet_title), getString(R.string.internet_message), getString(R.string.internet_button));
        }
    }

    private void addToListOfSearches(Search search) {
        mSearches.add(search);
        db.addSearch(search);
    }

    private void runArtistSearch() {
        Intent searchIntent = new Intent(Intent.ACTION_SYNC, null, getActivity(), SearchArtistIntentService.class);
        searchIntent.putExtra(Constants.SERVICE_SEARCH_TEXT, searchedText);
        //Result Receiver
        SearchArtistResultReceiver mReceiver = new SearchArtistResultReceiver(new Handler());
        mReceiver.setReciever(this);
        searchIntent.putExtra(Constants.SERVICE_RECEIVER, mReceiver);
        getActivity().startService(searchIntent);
    }

    private boolean connectedToInternet() {
        boolean connected = false;
        if (getActivity() != null && getActivity().getApplicationContext() != null) {
            ConnectivityManager cm = (ConnectivityManager)getActivity().getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo nInfo = cm.getActiveNetworkInfo();
            connected = nInfo != null && nInfo.isAvailable() && nInfo.isConnected();
        }
        return connected;
    }

    private void handleSendText(Intent intent) {
        String sharedText = intent.getStringExtra(Intent.EXTRA_TEXT);
        if (sharedText != null && searchEditText != null) {
            searchEditText.setText(sharedText);
        }
    }

    @Override
    public void onReceiveResult(int resultCode, Bundle resultData) {
        switch (resultCode) {
            case 0: // Call upucen
                //Add something
                searchButton.setText(R.string.searching);
                break;
            case 1:// Fetch result
                ArrayList<Musician> musicians = resultData.getParcelableArrayList(Constants.SERVICE_MUSICIANS);
                searchButton.setText(R.string.button_search);
                if (musicians != null) {
                    mData = musicians;
                    addToListOfSearches(new Search(searchedText, getTime(), true));
                    if (searchesDisplayed) {
                        changeView();
                    } else {
                        setMusicianAdapterData();
                    }
                }
                break;
            case 2:// Error
                searchButton.setText(R.string.button_search);
                addToListOfSearches(new Search(searchedText, getTime(), false));
                String error = resultData.getString(Intent.EXTRA_TEXT);
                Log.e(getString(R.string.error), error);
                showCustomDialog(getString(R.string.error), getString(R.string.error_text), getString(R.string.internet_button));
                break;
            default:
                throw new IllegalStateException("Unexpected value: " + resultCode);

        }
    }

    @Override
    public void onSearchClick(int position) {
        // Act like click on Search button
        this.searchedText = mSearches.get(position).getSearchText();
        search();
    }

    public interface OnMusicianClick {
        void onMusicianClicked(Musician musician);
    }

    @Override
    public void onMusicianClick(int position) {
        onMusicianClick.onMusicianClicked(mData.get(position));
    }

    private void showCustomDialog(String titleText, String messageText, String buttonText) {
        if (getContext() != null) {
            CustomDialog dialog = new CustomDialog(getContext(), titleText, messageText, buttonText);

            WindowManager.LayoutParams layoutParams = new WindowManager.LayoutParams();
            layoutParams.copyFrom(Objects.requireNonNull(dialog.getWindow()).getAttributes());
            layoutParams.width = WindowManager.LayoutParams.MATCH_PARENT;
            layoutParams.height = WindowManager.LayoutParams.WRAP_CONTENT;

            dialog.show();
            dialog.getWindow().setAttributes(layoutParams);
        }
    }

    private String getTime() {
        Date now = new Date();
        long timestamp = now.getTime();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy", getResources().getConfiguration().locale);
        SimpleDateFormat simpleTimeFormat = new SimpleDateFormat("HH:mm:ss XXX", getResources().getConfiguration().locale);

        return simpleDateFormat.format(timestamp) + " " + getString(R.string.time) + " " + simpleTimeFormat.format(timestamp);
    }

    private void changeView() {
        if (!searchesDisplayed) {
            setSearchesAdapterData();
            changeListButton.setText(R.string.show_musician_list);
        } else {
            setMusicianAdapterData();
            changeListButton.setText(R.string.show_searches);
        }
        searchesDisplayed = !searchesDisplayed;
    }

    private void setMusicianAdapterData() {
        //Linear Layout manager
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);
        // ADAPTER
        final RecyclerView.Adapter mAdapter = new MusicianAdapter(mData, this);
        recyclerView.setAdapter(mAdapter);
    }

    private void setSearchesAdapterData() {
        //Linear Layout manager
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);
        // ADAPTER
        final RecyclerView.Adapter mAdapter = new SearchAdapter(mSearches, this);
        recyclerView.setAdapter(mAdapter);
    }

}
