package com.ena.muratspahic.materijali_java.fragments;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import com.ena.muratspahic.materijali_java.R;
import com.ena.muratspahic.materijali_java.adapters.AlbumAdapter;
import com.ena.muratspahic.materijali_java.database.MusicianDBHelper;
import com.ena.muratspahic.materijali_java.model.Album;
import com.ena.muratspahic.materijali_java.utils.Constants;

import java.util.ArrayList;
import java.util.List;

public class ChildFragmentAlbums extends Fragment {

    private List<Album> mAlbums = new ArrayList<>();

    private ListView albumsListView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.child_fragment_five, container, false);

        TextView title = view.findViewById(R.id.textView);
        title.setText(getString(R.string.albums));

        albumsListView = view.findViewById(R.id.listView);

        if (getArguments() != null && getArguments().containsKey(Constants.MUSICIAN_SPOTIFY_ID)) {
            String musicianId = getArguments().getString(Constants.MUSICIAN_SPOTIFY_ID);
            if (musicianId != null) {
               // Get from database and set album adapter
                try (MusicianDBHelper db = new MusicianDBHelper(getActivity())) {
                    mAlbums = db.getAllAlbumsFromMusician(musicianId);
                    setAlbumsAdapter();
                } finally {
                    mAlbums = new ArrayList<>();
                }
            }
        }
        return view;
    }

    private void setAlbumsAdapter() {
        if (getActivity() != null && getActivity().getApplicationContext() != null) {
            AlbumAdapter adapter = new AlbumAdapter(mAlbums, getActivity().getApplicationContext());
            albumsListView.setAdapter(adapter);
            albumsListView.setOnItemClickListener((parent, v, position, id) -> openLinkInBrowser(position));
        }
    }

    private void openLinkInBrowser(int position) {
        Intent openOfficialPage = new Intent(Intent.ACTION_VIEW);
        openOfficialPage.setData(Uri.parse(mAlbums.get(position).getSpotifyLink()));
        startActivity(openOfficialPage);
    }
}
