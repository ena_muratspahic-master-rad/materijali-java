package com.ena.muratspahic.materijali_java.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.ena.muratspahic.materijali_java.R;
import com.ena.muratspahic.materijali_java.model.Song;

import java.util.List;

public class SongAdapter extends ArrayAdapter<Song> {

    private static class SongViewHolder {
        TextView numberTextView;
        TextView titleTextView;
    }

    public SongAdapter(List<Song> data, Context context) {
        super(context, R.layout.list_element_numbered, data);
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, @NonNull ViewGroup parent) {
        Song song = getItem(position);

        SongViewHolder viewHolder;
        final View view;

        if (convertView == null) {

            viewHolder = new SongViewHolder();
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.list_element_numbered, parent, false);
            viewHolder.numberTextView = convertView.findViewById(R.id.numberTextView);
            viewHolder.titleTextView = convertView.findViewById(R.id.titleTextView);
            view = convertView;

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (SongViewHolder) convertView.getTag();
            view = convertView;
        }

        if (song != null) {
            String number = "%s.";
            viewHolder.numberTextView.setText(String.format(String.valueOf(position+1),number));
            viewHolder.titleTextView.setText(song.getName());
        }

        return view;
    }

}