package com.ena.muratspahic.materijali_java.model;

import com.ena.muratspahic.materijali_java.R;

public class Genre {

    public static final int UNKNOWN = 0;

    public static String getGenreName() {
        return "Unknown";
    }

    static int getImageResource() {
        return R.drawable.unknown;
    }

    public static String getUnknownImageResource() {
        return "https://image.spreadshirtmedia.com/image-server/v1/mp/compositions/T1158A231MPA2501PT17X64Y28D1016489389S100/views/1,width=550,height=550,appearanceId=231,backgroundColor=CBCBCB,noPt=true,version=1572070041/unknown-word-black-sweatshirt-drawstring-bag.jpg";
    }

    static int getImageResource(String type) {
        if (type.toUpperCase().contains("POP")) {
            return R.drawable.pop;
        } else if (type.toUpperCase().contains("ELECTRO POP")
                || type.toUpperCase().contains("ELECTRO-POP")
                || type.toUpperCase().contains("ELECTROPOP")) {
            return R.drawable.electropop;
        } else if (type.toUpperCase().contains("ROCK")) {
            return R.drawable.rock;
        } else if (type.toUpperCase().contains("FUNK")) {
            return R.drawable.funk;
        } else if (type.toUpperCase().contains("INDIE POP")) {
            return R.drawable.indie_pop;
        } else {
            return R.drawable.unknown;
        }
    }
}
