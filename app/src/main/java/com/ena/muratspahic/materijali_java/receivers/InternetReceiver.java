package com.ena.muratspahic.materijali_java.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.widget.Toast;

import com.ena.muratspahic.materijali_java.R;

public class InternetReceiver extends BroadcastReceiver {

    public InternetReceiver() { }

    @Override
    public void onReceive(Context context, Intent intent) {
        if(intent.getAction() != null && intent.getAction().equals(ConnectivityManager.CONNECTIVITY_ACTION)) {
            ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo activeNetwork = connectivityManager.getActiveNetworkInfo();
            if (activeNetwork == null) {
                Toast.makeText(context, context.getString(R.string.internet_receiver_no_internet), Toast.LENGTH_LONG).show();
            }
        }
    }

}
