package com.ena.muratspahic.materijali_java.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.ena.muratspahic.materijali_java.R;

public class CustomDialog extends Dialog implements View.OnClickListener {

    private String title;
    private String message;
    private String buttonText;

    public CustomDialog(@NonNull Context context, String title, String message, String buttonText) {
        super(context);

        this.title = title;
        this.message = message;
        this.buttonText = buttonText;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_layout);
        setCancelable(false);

        ((TextView) findViewById(R.id.title)).setText(title);
        ((TextView) findViewById(R.id.message)).setText(message);
        Button button = findViewById(R.id.button);
        button.setText(buttonText);
        button.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        dismiss();
    }
}

